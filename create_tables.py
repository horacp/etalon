from peewee import PostgresqlDatabase

from db import Device, Measurement

# register models
models = [Device, Measurement]

db = PostgresqlDatabase(
    'postgres',
    user='postgres',
    host='localhost',
    password='sqljoke'
)

I_KNOW_PRECISELY_WHAT_I_AM_DOING = True

# connect to the db, drop all tables and recreate them
# ONLY RUN IN DEVELOPMENT
if I_KNOW_PRECISELY_WHAT_I_AM_DOING:
    # db.connect()
    db.drop_tables(models)
    db.create_tables(models)
