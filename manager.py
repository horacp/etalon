from collections import OrderedDict
from multiprocessing import Pool, cpu_count
from time import time

import numpy as np
import serial
from serial.serialutil import SerialException
from serial.tools import list_ports

from db import Channel, Device, Measurement
from utils import COLORS, Command, Status


def do(args: 'tuple[str, str]'):
    """
    This function handles measuring in in one process (on one port). It opens
    serial connection with the device and read its values.
    """
    # extract values from input tuple
    cmd = args[0]
    portName = args[1]

    # try:
    # open serial port (it will be closed on the last value is read)
    with serial.Serial(portName, baudrate=115200, timeout=0.9) as port:
        # write command
        port.write((cmd + '\n').encode())
        port.flush()

        # read values
        try:
            freq = float(port.readline().decode())
            diss = float(port.readline().decode())
            temp = float(port.readline().decode())
        except (serial.SerialException, ValueError) as err:
            print(err)
            return 'err'

    # return values in a tuple
    return (
        cmd,
        portName,
        freq,
        diss,
        temp
    )


class Manager:
    def __init__(self):
        self.pool = Pool(processes=cpu_count())
        self.measurements: 'OrderedDict[str, Measurement]' = OrderedDict()
        self.channels: 'OrderedDict[str, Channel]' = OrderedDict()
        self.indexor = 0

    def scan(self):
        # print([vars(p) for p in list_ports.comports()])

        for port in list_ports.comports():

            if port.manufacturer not in ['Teensyduino', 'Microsoft']:
                # skip
                continue

            # print(port)

            if port.device in self.channels:
                # port already initilized
                continue
            else:
                # find or create device based on serial number
                sn = port.serial_number
                name = 'QCM_' + sn
                device = Device.get_or_create(
                    serial_number=sn,
                    defaults={'name': name, 'serial_number': sn}
                )[0]

                # create a new channel
                self.channels[port.device] = Channel(
                    port.device,
                    device,
                    self.generate_color()
                )
        return self.channels

    def start(self, ports):
        """
        Start measuring on selected ports.
        """
        for port in ports:
            ch = self.channels[port]
            m = ch.start()
            self.measurements[m.id] = m

    def stop(self, ports):
        """
        Save data and stop measuring on selected ports.
        """
        for port in ports:
            ch = self.channels[port]
            ch.stop()

    def calibrate(self, ports):
        """
        Only run calibration on selected ports. Don't save any data.
        """
        for port in ports:
            ch = self.channels[port]
            ch.status = Status.CALIBRATING

    def generate_color(self):
        c = COLORS[self.indexor % len(COLORS)]
        self.indexor += 1
        return c

    def queue_handler(self):
        """
        A function should be executed once per second. It manages process pool,
        measurement and calibration. This corresponds to one measurement cycle.
        """
        current_time = round(time())
        data = list()

        # array of parameters for process pool
        values = []
        for ch in self.channels.values():
            if ch.status == Status.MEASURING:
                values.append((Command.MEASURE, ch.port))

        # execute everything in process pool
        # TODO: remove from channels
        try:
            results = self.pool.map(do, values)
        except SerialException:
            return

        for result in results:
            # skip on error
            if result == 'err':
                continue

            # unpack variables
            [cmd, port, freq, diss, temp] = result

            # find correct channel
            ch: Channel = self.channels.get(port, None)

            # check for existence of a channel and its status
            if not ch or ch.status != Status.MEASURING:
                continue

            # find correct measurement
            m = ch.measurement

            if cmd == Command.MEASURE:
                # set values
                ch.calib_freq = freq

                if freq != -1 and diss != -1 and temp != -1:
                    m.freq = np.append(m.freq, freq)
                    m.freqplot = np.append(m.freqplot, freq - m.offset['freq'])

                    m.diss = np.append(m.diss, diss)
                    m.dissplot = np.append(m.dissplot, diss - m.offset['diss'])

                    m.temp = np.append(m.temp, temp)
                    m.tempplot = np.append(m.tempplot, temp - m.offset['temp'])

                    m.time = np.append(m.time, current_time)

                # append to data that are emitted
                data.append([*result[1:], current_time])
            elif cmd == Command.CALIBRATE:
                # set calibration and emit
                ch.calib_freq = result[2]
