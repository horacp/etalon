import sys
import time
import os
from typing import Generator
import PySide2  # noqa: F401
import numpy as np
import pyqtgraph as pg
from PySide2 import QtCore, QtWidgets
from db import db, Device, Measurement
from manager import Manager
from utils import (sleep_time, today_timestamp, CSV_SEPARATOR,
                   format_timestamp, marker_types)
from datetime import datetime
from dateutil import tz
from scipy.signal import savgol_filter as sg
import utils as u

# set white background
pg.setConfigOption('background', 'w')


class NonScientificAxis(pg.AxisItem):
    def __init__(self, *args, **kwargs):
        super(NonScientificAxis, self).__init__(*args, **kwargs)

    def tickStrings(self, values, scale, spacing):
        return [
            int(value) if int(value) > 10000 else float(value)
            for value in values
        ]


class UnixTimeAxis(pg.AxisItem):
    def __init__(self, *args, **kwargs):
        super(UnixTimeAxis, self).__init__(*args, **kwargs)

    def tickStrings(self, values, scale, spacing):
        from_zone = tz.tzutc()
        to_zone = tz.tzlocal()
        try:
            return [
                datetime
                .utcfromtimestamp(value)
                .replace(tzinfo=from_zone)
                .astimezone(to_zone)
                .strftime('%H:%M:%S')
                for value in values
            ]
        except OSError:
            return values


class GameBoard(QtWidgets.QWidget):
    def __init__(self, manager: Manager):
        QtWidgets.QWidget.__init__(self)
        self.manager = manager

        scan_btn = QtWidgets.QPushButton("Scan")
        self.connect(scan_btn, QtCore.SIGNAL("clicked()"), self.scan)

        calibrate_btn = QtWidgets.QPushButton("Calibrate")

        start_btn = QtWidgets.QPushButton("Start")
        self.connect(start_btn, QtCore.SIGNAL("clicked()"), self.start)

        stop_btn = QtWidgets.QPushButton("Stop")
        self.connect(stop_btn, QtCore.SIGNAL("clicked()"), self.stop)

        reload_btn = QtWidgets.QPushButton("Reload")
        self.connect(reload_btn, QtCore.SIGNAL("clicked()"), self.reload)

        offset_btn = QtWidgets.QPushButton("Offset")
        self.connect(offset_btn, QtCore.SIGNAL("clicked()"), self.set_offset)

        export_btn = QtWidgets.QPushButton("Export")
        self.connect(export_btn, QtCore.SIGNAL("clicked()"), self.export)

        marker_btn = QtWidgets.QPushButton("Add Marker")
        self.connect(marker_btn, QtCore.SIGNAL("clicked()"), self.add_marker)

        self.ch_list = QtWidgets.QListWidget(self)
        self.ch_list.setFixedWidth(192)
        self.ch_list.setSelectionMode(
            QtWidgets.QAbstractItemView.ExtendedSelection
        )

        self.m_list = QtWidgets.QListWidget(self)
        self.m_list.setFixedWidth(256)
        self.m_list.setSelectionMode(
            QtWidgets.QAbstractItemView.ExtendedSelection
        )

        topLayout = QtWidgets.QHBoxLayout()
        topLayout.addWidget(scan_btn)
        topLayout.addWidget(start_btn)
        topLayout.addWidget(stop_btn)
        topLayout.addWidget(calibrate_btn)
        topLayout.addStretch(1)
        # topLayout.addWidget(reload_btn)
        topLayout.addWidget(offset_btn)
        topLayout.addWidget(marker_btn)
        topLayout.addWidget(export_btn)

        gridLayout = QtWidgets.QVBoxLayout()
        gridLayout.addLayout(topLayout)

        # this needs to be exactly like this, because axis cannot be shared
        # just make sure you update everything accordignly
        self.plotFreq = pg.PlotWidget(axisItems={
            'left': NonScientificAxis(orientation='left'),
            'bottom': UnixTimeAxis(orientation='bottom'),
        })
        self.plotDiss = pg.PlotWidget(axisItems={
            'left': NonScientificAxis(orientation='left'),
            'bottom': UnixTimeAxis(orientation='bottom'),
        })
        self.plotTemp = pg.PlotWidget(axisItems={
            'left': NonScientificAxis(orientation='left'),
            'bottom': UnixTimeAxis(orientation='bottom'),
        })

        self.plotsFreq: 'list[pg.PlotWidget]' = []
        self.plotsDiss: 'list[pg.PlotWidget]' = []
        self.plotsTemp: 'list[pg.PlotWidget]' = []

        plotsLayout = QtWidgets.QHBoxLayout()
        plotsLayout.addWidget(self.plotDiss, 3)
        plotsLayout.addWidget(self.plotTemp, 1)

        gridLayout.addWidget(self.plotFreq, 1)
        gridLayout.addLayout(plotsLayout, 1)

        mainLayout = QtWidgets.QHBoxLayout()
        mainLayout.addWidget(self.ch_list)
        mainLayout.addLayout(gridLayout, 1)
        mainLayout.addWidget(self.m_list)

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.refresh)
        self.timer.start(100)

        self.setLayout(mainLayout)

        # angle.setValue(60)
        # force.setValue(25)
        # angle.setFocus()

        # self.newGame()

    def refresh(self):
        ms = list(self.manager.measurements.values())
        for i in range(len(ms)):
            m: Measurement = ms[i]
            x = u.FILTER_OFFSET
            if len(m.time) >= u.FILTER_WINDOW:
                self.plotsFreq[i].setData(
                    m.time[x:-x],
                    sg(m.freqplot, u.FILTER_WINDOW, u.FILTER_ORDER)[x:-x]
                )
                self.plotsDiss[i].setData(
                    m.time[x:-x],
                    sg(m.dissplot, u.FILTER_WINDOW, u.FILTER_ORDER)[x:-x]
                )
                self.plotsTemp[i].setData(
                    m.time[x:-x],
                    sg(m.tempplot, u.FILTER_WINDOW, u.FILTER_ORDER)[x:-x]
                )

    @QtCore.Slot()
    def reload(self):
        ms = self.manager.measurements.values()
        self.plotFreq.clear()
        self.plotsFreq = []

        self.m_list.clear()
        for m in self.manager.measurements.values():
            self.m_list.addItem(m.data['name'])
        self.m_list.selectAll()

        self.ch_list.clear()
        for ch in self.manager.channels.values():
            self.ch_list.addItem(ch.device.name)
        self.ch_list.selectAll()

        for m in ms:
            m.freqplot = m.freq - m.offset['freq']
            m.dissplot = m.diss - m.offset['diss']
            m.tempplot = m.temp - m.offset['temp']

            self.plotsFreq.append(self.plotFreq.plot(pen={'color': m.color}))
            self.plotsDiss.append(self.plotDiss.plot(pen={'color': m.color}))
            self.plotsTemp.append(self.plotTemp.plot(pen={'color': m.color}))

            for marker in m.data.get('markers', []):
                def line():
                    return pg.InfiniteLine(
                        angle=90,
                        label=marker['name'],
                        pen=m.color,
                        pos=marker['timestamp'],
                        labelOpts={
                            'position': 0.1,
                            'color': m.color,
                            'movable': True
                        }
                    )
                self.plotFreq.addItem(line())
                self.plotDiss.addItem(line())
                self.plotTemp.addItem(line())

        self.refresh()

    @QtCore.Slot()
    def stop(self):
        chs = list(self.manager.channels.values())
        ports = []
        for i in self.ch_list.selectedIndexes():
            idx = i.row()
            ports.append(chs[idx].port)

        self.manager.stop(ports)
        self.reload()
        # self.plots[0].setData(np.random.rand(100))
        # self.plot.addItem(np.random.rand(100), pen={'color': 'k'})
        # self.plot.show()

    @QtCore.Slot()
    def export(self):
        if not os.path.exists('export'):
            os.mkdir('export')

        for m in self.selectedMeasurements():
            content = CSV_SEPARATOR.join([
                'Time',
                'Relative_Time',
                'Frequency',
                'Dissipation',
                'Temperature'
            ]) + '\n'
            for i in range(len(m.time)):
                content += CSV_SEPARATOR.join([
                    format_timestamp(m.time[i]),
                    str(i),
                    str(m.freq[i]),
                    str(m.diss[i]),
                    str(m.temp[i]),
                ])
                content += '\n'

            file = os.path.join('export', m.data['name'] + '.csv')
            with open(file, 'w') as f:
                f.write(content)

    @QtCore.Slot()
    def start(self):
        chs = list(self.manager.channels.values())
        ports = []
        for i in self.ch_list.selectedIndexes():
            idx = i.row()
            ports.append(chs[idx].port)

        self.manager.start(ports)
        self.reload()

    @QtCore.Slot()
    def scan(self):
        self.manager.scan()
        self.reload()

    @QtCore.Slot()
    def add_marker(self):
        self.form = QtWidgets.QWidget()
        self.form.setWindowTitle('Add Marker')
        self.form.setGeometry(100, 100, 300, 200)

        hh = QtWidgets.QSpinBox()
        mm = QtWidgets.QSpinBox()
        ss = QtWidgets.QSpinBox()
        name = QtWidgets.QComboBox()
        name.addItems(marker_types)
        comment = QtWidgets.QLineEdit()

        @QtCore.Slot()
        def marker():
            new_marker = {
                'name': name.currentText(),
                'timestamp': today_timestamp(hh.text(), mm.text(), ss.text()),
                'comment': comment.text()
            }
            for m in self.selectedMeasurements():
                m.data['markers'].append(new_marker)

                q = Measurement \
                    .update(data=m.data) \
                    .where(Measurement.id == m.id)
                q.execute()

            self.reload()
            self.form.close()

        btn = QtWidgets.QPushButton('Save')
        self.connect(btn, QtCore.SIGNAL("clicked()"), marker)

        time_layout = QtWidgets.QHBoxLayout()
        time_layout.addWidget(hh)
        time_layout.addWidget(mm)
        time_layout.addWidget(ss)

        layout = QtWidgets.QVBoxLayout()
        layout.addLayout(time_layout)
        layout.addWidget(name)
        layout.addWidget(comment)
        layout.addWidget(btn)

        self.form.setLayout(layout)
        self.form.show()

    @QtCore.Slot()
    def set_offset(self):
        self.offset_form = QtWidgets.QWidget()
        self.offset_form.setWindowTitle('Set offset')
        self.offset_form.setGeometry(100, 100, 300, 200)
        hh = QtWidgets.QSpinBox()
        mm = QtWidgets.QSpinBox()
        ss = QtWidgets.QSpinBox()

        @QtCore.Slot()
        def offset():
            for m in self.selectedMeasurements():
                try:
                    ts = today_timestamp(hh.text(), mm.text(), ss.text())
                    idx = np.where(m.time == ts)[0]
                    m.offset['freq'] = m.freq[idx[0]]
                    m.offset['diss'] = m.diss[idx[0]]
                    # m.offset['temp'] = m.temp[idx[0]]
                except IndexError:
                    m.offset['freq'] = 0
                    m.offset['diss'] = 0
                    m.offset['temp'] = 0

            self.reload()
            self.offset_form.close()

        @QtCore.Slot()
        def offset2():
            for m in self.selectedMeasurements():
                m.offset['freq'] = 0
                m.offset['diss'] = 0
                m.offset['temp'] = 0

            self.reload()
            self.offset_form.close()

        btn = QtWidgets.QPushButton('Save')
        self.connect(btn, QtCore.SIGNAL("clicked()"), offset)

        btn2 = QtWidgets.QPushButton('Remove offset')
        self.connect(btn2, QtCore.SIGNAL("clicked()"), offset2)

        time_layout = QtWidgets.QHBoxLayout()
        time_layout.addWidget(hh)
        time_layout.addWidget(mm)
        time_layout.addWidget(ss)

        layout = QtWidgets.QVBoxLayout()
        layout.addLayout(time_layout)
        layout.addWidget(btn)
        layout.addWidget(btn2)

        self.offset_form.setLayout(layout)
        self.offset_form.show()

    def selectedMeasurements(self) -> Generator[Measurement, None, None]:
        ms = list(self.manager.measurements.values())
        for i in self.m_list.selectedIndexes():
            idx = i.row()
            yield ms[idx]


class Ticker(QtCore.QThread):
    def __init__(self, manager: Manager) -> None:
        super().__init__()
        self.manager = manager

    def run(self):
        """
        This function should be called async and it handles 'ticking',
        a method that calls some actions exactly once per second. The
        milliseconds are always zero, as the function calls are performed
        every whole second.
        """
        # wait for whole second
        time.sleep(sleep_time())

        # run forever
        while True:
            # call handler
            self.manager.queue_handler()

            # wait for whole second and print exec time
            t = sleep_time()
            # print('Execution time:', int(
            #     (1 - t) * 1000), 'ms', self.manager.channels)
            time.sleep(t)


if __name__ == '__main__':
    if not os.path.exists('db.sqlite3'):
        db.create_tables([Device, Measurement])

    manager = Manager()

    app = QtWidgets.QApplication(sys.argv)

    ticker = Ticker(manager)
    ticker.start()

    board = GameBoard(manager)
    board.setGeometry(1800, 300, 600, 600)
    board.setWindowTitle('Etalon Software')
    board.show()

    sys.exit(app.exec_())
