# Etalon Software

## Installation

### Linux

Create virtual environment and install dependencies:

```sh
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

After that, you should initialize database:

```sh
python create_tables.py
```

### Windows

God help you!

## Make an executable

```sh
python pyinstaller --one-file app.py # maybe...
```
