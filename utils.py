import datetime
import os
import time

INITIAL_FREQ = 10**7
INTERVAL_CALIB = 10**5
INTERVAL_HALF = 10**4 // 1
INTERVAL_STEP = 40
POLYFIT_COEFFICIENT = 0.95
DISSIPATION_PERCENT = 0.707
CSV_SEPARATOR = ','
DATE_SEPARATOR = '_'
RAW_DATA_SEPARATOR = ';'
OUTPUT_EXT = 'output'
RAW_DATA_EXT = 'txt'
CURRENT_DIR = os.getcwd()
COLORS = [
    (0, 0, 255),
    (255, 0, 0),
    (0, 255, 0),
    (255, 0, 255),
    (0, 255, 255),
    (0, 128, 128),
    (0, 0, 128),
    (128, 0, 128),
    (0, 128, 0),
    (128, 0, 0),
]
FILTER_WINDOW = 33
FILTER_ORDER = 2
FILTER_OFFSET = (FILTER_WINDOW // 2) - 1

marker_types = [
    'PBS injection',
    'PBS**  injection',
    'Q injection',
    'sample injection',
    'deactivation',
    'activation',
    'antibody immobilization',
    'air on the surface',
    'buffer ready',
    'sample ready',
    'air corrected',
    'flow rate change',
    'temperature change',
    'mechanical disturbance',
    'cell disconnection',
    'signal change - uknown reason',
]


class Status:
    """
    Available statuses of the device.
    """
    READY = 1
    CALIBRATING = 2
    CALIBRATED = 3
    CALIB_ERR = 4
    MEASURING = 5


class Command:
    """
    Supported commands for the firmware.
    """
    CALIBRATE = 'c'
    MEASURE = 'm'


def timestamp(date=datetime.datetime.now()):
    """
    Get UNIX timestamp in milliseconds (required by JS).
    """
    return int(date.timestamp() * 1000)


def format_unix_timestamp(millis):
    """
    Format unix timestamp from milliseconds.
    """
    return datetime.datetime.fromtimestamp(millis//1000) \
        .strftime('%Y-%m-%d{}%H-%M-%S'.format(DATE_SEPARATOR))


def format_timestamp(seconds):
    """
    Format unix timestamp from milliseconds.
    """
    return datetime.datetime.fromtimestamp(seconds) \
        .strftime('%Y-%m-%d{}%H-%M-%S'.format(DATE_SEPARATOR))


def readable_timestamp():
    """
    Format unix timestamp.
    """
    return datetime.datetime.fromtimestamp(time.time()) \
        .strftime('%Y-%m-%d{}%H-%M-%S'.format(DATE_SEPARATOR))


def sleep_time():
    """
    Return time in milliseconds to the mearest whole second.
    """
    return (999 - round(time.time() * 1000) % 1000) / 1000


def today_timestamp(hour, minute, second):
    today = datetime.datetime.now()
    that = datetime.datetime(
        year=today.year,
        month=today.month,
        day=today.day,
        hour=int(hour),
        minute=int(minute),
        second=int(second)
    )

    return int(that.timestamp())
