import sys
import time
import os
from typing import Generator
import PySide2  # noqa: F401
import numpy as np
import pyqtgraph as pg
from PySide2 import QtCore, QtWidgets
from db import db, Device, Measurement
from utils import (sleep_time, today_timestamp, CSV_SEPARATOR,
                   format_unix_timestamp, marker_types)
from datetime import datetime
from dateutil import tz


# set white background
pg.setConfigOption('background', 'w')


class NonScientificAxis(pg.AxisItem):
    def __init__(self, *args, **kwargs):
        super(NonScientificAxis, self).__init__(*args, **kwargs)

    def tickStrings(self, values, scale, spacing):
        return [
            int(value)
            for value in values
        ]


class UnixTimeAxis(pg.AxisItem):
    def __init__(self, *args, **kwargs):
        super(UnixTimeAxis, self).__init__(*args, **kwargs)

    def tickStrings(self, values, scale, spacing):
        from_zone = tz.tzutc()
        to_zone = tz.tzlocal()
        try:
            return [
                datetime
                .utcfromtimestamp(value)
                .replace(tzinfo=from_zone)
                .astimezone(to_zone)
                .strftime('%H:%M:%S')
                for value in values
            ]
        except OSError:
            return values


class Epp(QtWidgets.QWidget):
    def __init__(self):
        QtWidgets.QWidget.__init__(self)

        self.marker_list = QtWidgets.QListWidget(self)
        self.marker_list.setFixedWidth(256)
        self.marker_list.setSelectionMode(
            QtWidgets.QAbstractItemView.ExtendedSelection
        )

        self.plot_widget = pg.PlotWidget(axisItems={
            'left': NonScientificAxis(orientation='left'),
            'bottom': UnixTimeAxis(orientation='bottom'),
        })
        self.plot = self.plot_widget.plot()

        load_btn = QtWidgets.QPushButton("Load file")
        self.connect(load_btn, QtCore.SIGNAL("clicked()"), self.loadFile)

        marker_btn = QtWidgets.QPushButton("Add Marker")
        self.connect(marker_btn, QtCore.SIGNAL("clicked()"), self.add_marker)

        marke_btn = QtWidgets.QPushButton("Remove Marker")
        self.connect(marke_btn, QtCore.SIGNAL("clicked()"), self.remove_marker)

        topLayout = QtWidgets.QHBoxLayout()
        topLayout.addWidget(load_btn)
        topLayout.addStretch(1)
        topLayout.addWidget(marker_btn)
        topLayout.addWidget(marke_btn)

        gridLayout = QtWidgets.QVBoxLayout()
        gridLayout.addLayout(topLayout)
        gridLayout.addWidget(self.plot_widget)

        mainLayout = QtWidgets.QHBoxLayout()
        mainLayout.addLayout(gridLayout, 1)
        mainLayout.addWidget(self.marker_list)

        self.setLayout(mainLayout)

    def reload(self):
        self.plot_widget.clear()
        self.plot = self.plot_widget.plot()
        self.plot.setData(
            self.m.time,
            self.m.freq,
            pen={'color': self.m.color}
        )

        items = []
        for marker in self.m.data['markers']:
            line = pg.InfiniteLine(
                angle=90,
                label=marker['name'],
                pen=self.m.color,
                pos=marker['timestamp'],
                labelOpts={
                    'position': 0.1,
                    'color': self.m.color,
                    'movable': True
                }
            )

            from_zone = tz.tzutc()
            to_zone = tz.tzlocal()
            items.append(datetime
                         .utcfromtimestamp(marker['timestamp'])
                         .replace(tzinfo=from_zone)
                         .astimezone(to_zone)
                         .strftime('%H:%M:%S') + ' - ' + marker['name'])
            self.plot_widget.addItem(line)

        self.marker_list.clear()
        self.marker_list.addItems(items)

    @QtCore.Slot()
    def loadFile(self):
        fname = QtWidgets.QFileDialog.getOpenFileName()[0]
        if not fname:
            return

        self.m = Measurement(None, 'b', mname=os.path.basename(fname))
        self.ddd = ''
        with open(fname, 'r') as file:
            lines = file.readlines()
            sep = ';' if ';' in lines[0] else ','
            for line in lines[1:]:
                [ddd, ttt, rrr, t, f, d] = line.split(sep)
                if not self.ddd:
                    self.ddd = ddd
                [yyyy, mm, dd] = ddd.split('-')
                [H, M, S] = ttt.split(':')
                [_, SS] = rrr.replace(',', '.').split('.')
                dt = datetime(
                    year=int(yyyy),
                    month=int(mm),
                    day=int(dd),
                    hour=int(H),
                    minute=int(M),
                    second=int(S),
                    microsecond=int(SS) * 1000
                )
                self.m.freq = np.append(
                    self.m.freq, float(f.replace(',', '.')))
                self.m.diss = np.append(
                    self.m.diss, float(d.replace(',', '.')))
                self.m.temp = np.append(
                    self.m.temp, float(t.replace(',', '.')))
                self.m.time = np.append(self.m.time, dt.timestamp())

        self.m.save()
        self.reload()

    @QtCore.Slot()
    def remove_marker(self):
        for idx in self.selectedMarkers():
            self.m.data['markers'].pop(idx)

        q = Measurement \
            .update(data=self.m.data) \
            .where(Measurement.id == self.m.id)
        q.execute()
        self.reload()

    @QtCore.Slot()
    def add_marker(self):
        self.form = QtWidgets.QWidget()
        self.form.setWindowTitle('Add Marker')
        self.form.setGeometry(100, 100, 300, 200)

        hh = QtWidgets.QSpinBox()
        mm = QtWidgets.QSpinBox()
        ss = QtWidgets.QSpinBox()
        name = QtWidgets.QComboBox()
        name.addItems(marker_types)
        comment = QtWidgets.QLineEdit()

        @QtCore.Slot()
        def marker():
            [yyyy, mM, dd] = self.ddd.split('-')
            dt = datetime(
                year=int(yyyy),
                month=int(mM),
                day=int(dd),
                hour=int(hh.text()),
                minute=int(mm.text()),
                second=int(ss.text()),
            )
            new_marker = {
                'name': name.currentText(),
                'timestamp': dt.timestamp(),
                'comment': comment.text()
            }
            self.m.data['markers'].append(new_marker)

            q = Measurement \
                .update(data=self.m.data) \
                .where(Measurement.id == self.m.id)
            q.execute()

            self.reload()
            self.form.close()

        btn = QtWidgets.QPushButton('Save')
        self.connect(btn, QtCore.SIGNAL("clicked()"), marker)

        time_layout = QtWidgets.QHBoxLayout()
        time_layout.addWidget(hh)
        time_layout.addWidget(mm)
        time_layout.addWidget(ss)

        layout = QtWidgets.QVBoxLayout()
        layout.addLayout(time_layout)
        layout.addWidget(name)
        layout.addWidget(comment)
        layout.addWidget(btn)

        self.form.setLayout(layout)
        self.form.show()

    def selectedMarkers(self) -> Generator[dict, None, None]:
        for i in self.marker_list.selectedIndexes():
            yield i.row()


if __name__ == '__main__':
    if not os.path.exists('db.sqlite3'):
        db.create_tables([Device, Measurement])

    app = QtWidgets.QApplication(sys.argv)

    board = Epp()
    board.setGeometry(300, 300, 1000, 600)
    board.setWindowTitle('Etalon Software')
    board.show()

    sys.exit(app.exec_())
