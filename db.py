import json
import os

import numpy as np
from peewee import (Field, ForeignKeyField, Model,  # noqa: E501, F401
                    OperationalError, SqliteDatabase, TextField)
from playhouse.sqlite_ext import JSONField

from utils import CURRENT_DIR, Status, readable_timestamp

db = SqliteDatabase('db.sqlite3', pragmas={
    'journal_mode': 'wal',
    'cache_size': -1 * 64000,  # 64MB
    'foreign_keys': 1,
    'ignore_check_constraints': 0,
    'synchronous': 0
})


class RealArrayField(Field):
    """
    Native Postgres real array, that is converted to the NumPy float32 array.
    """
    field_type = 'REAL[]'

    def db_value(self, value) -> str:
        # perform fast serialization
        return '{' + ','.join([str(x) for x in value]) + '}'

    def python_value(self, value) -> 'list[float]':
        # perform converting to the NumPy array
        return np.array(
            [float(x) for x in value[1:-1].split(',')]
            if not isinstance(value, list) else value,
            dtype=np.float32
        )


class IntegerArrayField(Field):
    """
    Native Postgres int array, that is converted to the NumPy int2 array.
    """
    field_type = 'INT[]'

    def db_value(self, value) -> str:
        # perform fast serialization
        return '{' + ','.join([str(x) for x in value]) + '}'

    def python_value(self, value) -> 'list[int]':
        # perform converting to the NumPy array
        return np.array(
            [int(x) for x in value[1:-1].split(',')]
            if not isinstance(value, list) else value,
            dtype=np.int32
        )


class BaseModel(Model):
    """
    Base class for all models, that only registers database.
    """
    class Meta:
        database = db


class Device(BaseModel):
    """
    A model for storing device information.
    """
    name = TextField()
    serial_number = TextField(unique=True)

    def __dict__(self) -> dict:
        return {
            'id': str(self.id),
            'name': self.name,
            'serialNumber': self.serial_number,
        }


class Channel():
    """
    Handles management of one serial port.
    """

    def __init__(self, port: str, device: Device, color) -> None:
        self.calib_freq = 10**7
        self.port = port
        self.device = device
        self.status = Status.READY
        self.measurement: Measurement = None
        self.color = color

    def start(self) -> 'Measurement':
        m = Measurement(self.device, self.color)
        m.save()

        self.status = Status.MEASURING
        self.measurement = m

        return m

    def stop(self) -> None:
        if self.measurement:
            self.measurement.save()
        self.measurement = None
        self.status = Status.READY

    def __dict__(self) -> dict:
        m = self.measurement
        return {
            'calibFreq': self.calib_freq,
            'port': self.port,
            'device': self.device.__dict__(),
            'status': self.status,
            'measurementId': str(m.id) if m else None
        }

    def __del__(self):
        if self.measurement:
            self.measurement.save()


class Measurement(BaseModel):
    """
    A model for measurement. Saves all needed information.
    """
    device = ForeignKeyField(Device, backref='measurements', null=True)
    data = JSONField()
    freq = RealArrayField()
    diss = RealArrayField()
    temp = RealArrayField()
    time = RealArrayField()

    def __init__(self, device: Device, color, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.device = device
        self.freq = np.array([])
        self.diss = np.array([])
        self.temp = np.array([])
        self.freqplot = np.array([])
        self.dissplot = np.array([])
        self.tempplot = np.array([])
        self.time = np.array([])
        self.data = {
            'name': kwargs.get('mname', None) or readable_timestamp() + ' ' + str(getattr(device, 'name', device)),  # noqa: 501
            'markers': list()
        }
        self.color = color
        self.offset = {
            'freq': 0,
            'diss': 0,
            'temp': 0,
            'time': 0,
        }

    def save(self, offline=False, force_insert=False, only=None):
        data_path = os.path.join(CURRENT_DIR, 'data')
        file_path = os.path.join(data_path, self.data.get('name') + '.json')  # noqa: 501

        try:
            if self.id == -1:
                self.id = None

            returnable = super().save(force_insert=force_insert, only=only)

            if os.path.exists(file_path):
                os.unlink(file_path)

            return returnable
        except OperationalError:  # noqa: E722
            if not self.id:
                self.id = -1

            if offline:
                return None

            if not os.path.exists(data_path):
                os.mkdir(data_path)

            with open(file_path, 'w') as f:
                m = self.__dict__()
                f.write(json.dumps(m))

            return None

    def __dict__(self) -> dict:
        return {
            'id': str(self.id),
            'data': self.data,
            'device': self.device.__dict__(),
            'values': {
                'freq': self.freq.tolist(),
                'diss': self.diss.tolist(),
                'temp': self.temp.tolist(),
                'time': self.time.tolist(),
            }
        }

    def __del__(self):
        self.save()
